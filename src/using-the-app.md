# Using the app

:memo: **Note:**
This user guide refers to a Hexagon prototype that is currently under development. **Not all the functionalities listed here have been implemented as of yet**, and changes to the UX/UI design are still possible, based on the feedback we receive from early testers.

Aside from limitations related to the early stage of Hexagon's development, the primary constraint the Hexagon is facing now is the lack of ETH rewards withdrawals. This functionality depends on the [Ethereum Shanghai upgrade][shanghai-upgrade] which is tentatively planned for March 2023.
  
The alpha version of the app, with limited but quickly expanding functionality, has been deployed to the [Görli Testnet][gorli]. If you would like to help us test Hexagon, add Görli to your Metamask wallet by following the instruction in this [guide][gorli-metamask]. 
 
To help us test the app you will also need some TestGLM. In order to get them, go to our [Etherscan Faucet][test-glm-faucet], click on the `Contract` tab, then choose `Write Contract` and `Connect to Web3`. You will be asked to connect your Metamask wallet. Please note that to receive TestGLM you need to have some Görli ETH in your wallet. You can get them for free from the [Görli Faucet][gorli-faucet].

Once you have connected your wallet click on the `sendMeGLM` function and choose `Write`. Metamask will ask you to confirm the transaction. Soon after you will recieve your TestGLM. You can now [launch Hexagon][launching-hexagon], and start testing. 

For testing purposes the epoch length is currently set to 10 minutes and we are usuing historical Moon missions as placeholders for  projects eligible to receive Hexagon funding.
 
You can share your experience with the alpha version of the app on a dedicated [Discord channel][discord].
:memo: 

## Getting started

The first thing you need to do after [launching the app][launching-hexagon] is to connect your wallet to it. At the moment, only [Metamask][metamask] is supported, but we are working on integrating other popular wallets as well.

To connect your Metamask wallet click on the `Connect wallet` button on the top of the screen and follow the instructions in your wallet.

Once you have connected your wallet, you will have to stake some GLM in the `Earn` tab. GLM is an ERC-20 Ethereum-based token native to the [Golem Network][golem-network]. If you do not have any GLM you can acquire them at various [exchanges][exchanges] and on [Uniswap][uniswap]. 

During the testnet phase of Hexagon development we are using TestGLM, which you can acquire by completing the steps described above.

You can stake as low as 1 GLM, but to be able to claim user rewards and allocate your funds to eligible projects, you need to stake at least 100 GLM for one 90-day epoch.

The more GLM you stake, the higher your user reward, and the more funds you can allocate to your favorite projects. Check Hexagon’s [Technical Outline][technical-outline], if you want to know more about how we calculate rewards and matched funds.

Hexagon's GLM staking mechanism is non-custodial. You are in full control of your funds. You can unstake your GLM at any time. Please note, however, that if you unstake your GLM before the current epoch ends, you will not be able to claim your user rewards or allocate any funds to your favorite projects.

<video src="./videos/wallet.mp4" playsInline controls></video>

You can access your Hexagon-connected wallet at any time by clicking on a tile showing your Ethereum address at the top of the screen.

The `Wallet` view allows you to check your connected wallet balances in ETH, GLM, and their fiat equivalents in the currency of your choice. The view also displays your rewards budget, as well as pending allocations and withdrawals.

The `Wallet` view also allows you to disconnect your wallet from Hexagon at any time.

## The main app views

The app has 5 main views, which you can access by clicking at the appropriate tab at the bottom of the screen:
- `Projects` - where you can scroll through the list of projects to which you can donate in the current epoch;
- `Allocate` - which allows you to allocate your user rewards, in part or full, to projects you shortlisted;
- `Metrics` - which shows you a range of stats about the current and previous epochs to help you make allocation decisions;
- `Earn` - where you can edit your GLM stake, withdraw your user rewards in ETH, and check current and estimated reward stats;
- `Settings` - which allows you to access various app settings. The main toggle lets you to choose ETH or a selected fiat as a primary display currency.

## Allocate your Hexagon funds

<video src="./videos/allocate.mp4" playsInline controls></video>

To find out more about the causes which were shortlisted by the Hexagon community as potential donation recipients in the current epoch, click on the `Projects` tab at the bottom of the screen. (Information on the eligibility criteria and on how users can submit a project can be found [here][propose-a-project]).

You can scroll through the list of eligible projects and see how many funds have been already allocated to them by Hexagon community members. If you would like to know more about a project, click on it to read a more detailed description, and to see the list of the latest donations Hexagon community members have made to it during the current epoch.

If you find a project you would like to support with your User Rewards, click on the `Add to Allocate` button right next to it.

Currently, you can only choose one project to which you would like to allocate some part of your user rewards, but we are working on making it possible to split your donation between several projects.

Projects you shortlisted for allocation will appear in the `Allocate` tab.

In the `Allocate` view, you can see the projects you added to your shortlist as well as some metrics that can help you make allocation decisions. The ETH amount you see next to the project indicates how much money Hexagon users have already allocated to it during the current epoch. Percentages show you the current distribution of rewards among projects, and the dots indicate whether the project has passed a predefined threshold of individual donations. The green dot means that a project is above the threshold, and thus is eligible to receive funding. The red dot means that a project is bellow the treshold. Keep in mind that [ETH rewards allocated to projects that didn’t pass the funding threshold will go back to Golem Foundation][how-it-works].    

Click on a shortlisted project to decide how much you would like to donate to it. Once you've chosen the correct amount click `Done`, and then `Allocate`. You will be shown an overview of your allocated decisions in the current epoch. You will be able to `Confirm` your decision or `Edit` your allocations further.

## Staking and withdrawing funds

<video src="./videos/staking.mp4" playsInline controls></video>

The `Earn` tab allows you to edit your GLM stake, withdraw your user rewards in ETH, and check the current and estimated reward stats.

Your `Current Epoch Stake` will be displayed there. By clicking on the `Edit GLM Stake` button you can change the amount of tokens you would like to stake. Choose the amount you would like to stake and click `Done`. You can now approve your stake.

Once you've approved your stake, you will be shown a `History` view where you will see your stakes, funds you allocated to different projects, and user rewards you withdrew.

You can change your stake at any moment, please keep in mind, however, that:
- to be eligible for user rewards and to be able to donate to your preferred projects you have to stake at least 100 GLM for a whole epoch;
- if you lower your stake, your user reward will be calculated in proportion to the smaller amount.

## Metrics

<video src="./videos/metrics.mp4" playsInline controls></video>

In the `Metrics` view you can check the following information:
- when the current epoch allocation period ends;
- the total amount of ETH staked by the Golem Foundation and GLM staked by Hexagon users;
- the fraction of GLM staked by Hexagon users in relation to the Total GLM supply;
- the current value of ETH rewards transferred by Golem Foundation to Hexagon, and the value of users and matched donations to community chosen projects; 
- how much money has been claimed by community users versus donated to eligible projects;
- information on the amount of GLM tokens that are unclaimed and unallocated at the end of the epoch, or allocated to projects below the cutoff threshold. 

[shanghai-upgrade]: https://github.com/ethereum/execution-specs/blob/master/network-upgrades/mainnet-upgrades/shanghai.md
[gorli]: https://goerli.net/
[gorli-metamask]: https://www.tech-zone.co.uk/Guides/how-to-add-goerli-network-to-metamask
[test-glm-faucet]: https://goerli.etherscan.io/address/0x7236eA6198F4eF69355DC5CB25F7f09E5439f754
[gorli-faucet]: https://goerlifaucet.com/ 
[launching-hexagon]: https://TBA
[metamask]: https://metamask.io/
[discord]: https://TBA
[golem-network]: https://golem.network
[exchanges]: https://glm.golem.network/
[uniswap]: https://app.uniswap.org/#/swap?outputCurrency=0x7DD9c5Cba05E151C895FDe1CF355C9A1D5DA6429
[propose-a-project]: ./propose-a-project.md
[technical-outline]: ./technical-outline.md
[how-it-works]: ./how-it-works.md 
